var personas = [];


var persona1 = {};
persona1.nombre = 'Arturo';
persona1.edad = 27;
persona1.estatura = 1.75;
persona1.carro = false;

var persona2 = {};
persona2.nombre = 'Carlos';
persona2.edad = 30;
persona2.estatura = 1.65;
persona2.carro = false;

var persona3 = {};
persona3.nombre = 'Daniel';
persona3.edad = 25;
persona3.estatura = 1.63;
persona3.carro = true;

var persona4 = {};
persona4.nombre = 'Diana';
persona4.edad = 29;
persona4.estatura = 1.68;
persona4.carro = true;

var persona5 = {};
persona5.nombre = 'Edson';
persona5.edad = 23;
persona5.estatura = 1.79;
persona5.carro = false;

var persona6 = {};
persona6.nombre = 'Javier';
persona6.edad = 19;
persona6.estatura = 1.80;
persona6.carro = false;

personas.push(persona1);
personas.push(persona2);
personas.push(persona3);
personas.push(persona4);
personas.push(persona5);
personas.push(persona6);
console.log(personas.length);
console.log(personas);

personas.forEach(function(element, index){
	console.log((index+1) + '.- ' + element.nombre);
	console.log((index+1) + '.- ' + element.edad);
	console.log((index+1) + '.- ' + element.estatura);
	console.log((index+1) + '.- ' + element.carro);
})∫
